import { Sprite, loader, Container } from "pixi.js";

export class World extends Container{

    sprite: Sprite
    base: Container

    constructor(){
        super()
        this.sprite = new Sprite(loader.resources["world"].texture)

        this.sprite.anchor.set(0.5, 0.5)
        this.sprite.scale.set(0.5, 0.5)

        this.base = new Container()
        this.base.position.set(window.innerWidth / 2, window.innerHeight / 2)
        this.base.addChild(this.sprite)

        this.position.set(
            -this.sprite.texture.width / 2,
            -this.sprite.texture.height / 2
        )
        this.sprite.addChild(this)
    }

}