import "hammerjs"
import { Point } from "pixi.js";
import { World } from "./world";

export class GestureHandler{

    hammer: HammerManager

    initialWorldPos = new Point()
    initialWorldScale = new Point()

    constructor(element: HTMLElement, world: World){
        this.hammer = new Hammer(element)

        this.hammer.get("pan").set({direction: Hammer.DIRECTION_ALL})
        this.hammer.get("pinch").set({enable: true})

        this.hammer.on("pan", event => {
            world.sprite.x = this.initialWorldPos.x + event.deltaX
            world.sprite.y = this.initialWorldPos.y + event.deltaY
        })

        this.hammer.on("panstart", event => {
            this.initialWorldPos.copy(world.sprite.position)
        })

        this.hammer.on("pinchstart", event => {
            const local = world.base.toLocal(new Point(event.center.x, event.center.y))

            world.base.x += local.x
            world.base.y += local.y

            world.sprite.anchor.x += (local.x - world.sprite.x) / world.sprite.width
            world.sprite.anchor.y += (local.y - world.sprite.y) / world.sprite.height

            world.sprite.position.set(0, 0)
            
            world.position.set(
                -world.sprite.anchor.x * world.sprite.texture.width,
                -world.sprite.anchor.y * world.sprite.texture.height
            )

            this.initialWorldPos.copy(world.sprite.position)
            this.initialWorldScale.copy(world.sprite.scale)
        })

        this.hammer.on("pinch", event => {
            world.sprite.x = this.initialWorldPos.x + event.deltaX
            world.sprite.y = this.initialWorldPos.y + event.deltaY
            
            world.sprite.scale.x = this.initialWorldScale.x * event.scale
            world.sprite.scale.y = this.initialWorldScale.y * event.scale

            if(world.sprite.scale.x > 1) world.sprite.scale.x = 1
            if(world.sprite.scale.y > 1) world.sprite.scale.y = 1

            if(world.sprite.scale.x < 0.25) world.sprite.scale.x = 0.25
            if(world.sprite.scale.y < 0.25) world.sprite.scale.y = 0.25
        })

        element.addEventListener("wheel", event => {
            const local = world.base.toLocal(new Point(event.clientX, event.clientY))

            world.base.x += local.x
            world.base.y += local.y

            world.sprite.anchor.x += (local.x - world.sprite.x) / world.sprite.width
            world.sprite.anchor.y += (local.y - world.sprite.y) / world.sprite.height

            world.sprite.position.set(0, 0)
            
            world.position.set(
                -world.sprite.anchor.x * world.sprite.texture.width,
                -world.sprite.anchor.y * world.sprite.texture.height
            )

            world.sprite.scale.x -= event.deltaY * 0.05
            world.sprite.scale.y -= event.deltaY * 0.05

            if(world.sprite.scale.x > 1) world.sprite.scale.x = 1
            if(world.sprite.scale.y > 1) world.sprite.scale.y = 1

            if(world.sprite.scale.x < 0.25) world.sprite.scale.x = 0.25
            if(world.sprite.scale.y < 0.25) world.sprite.scale.y = 0.25
        })
    }

}